<?php 
include_once('transporte.php');

class patineta extends transporte{
    private $llanta;

    //sobreescritura de constructor
    public function __construct($nom,$vel,$com,$llanta){
        parent::__construct($nom,$vel,$com);
        $this->llanta=$llanta;
    }

    // sobreescritura de metodo
    public function resumenPatineta(){
        $mensaje=parent::crear_ficha();
        $mensaje.='<tr>
                    <td>Numero de Llantas:</td>
                    <td>'. $this->llanta.'</td>				
                </tr>';
        return $mensaje;
    }
}

